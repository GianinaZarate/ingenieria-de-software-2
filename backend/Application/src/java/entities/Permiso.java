/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "permiso")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Permiso.findAll", query = "SELECT p FROM Permiso p")
    , @NamedQuery(name = "Permiso.findByIdPermiso", query = "SELECT p FROM Permiso p WHERE p.permisoPK.idPermiso = :idPermiso")
    , @NamedQuery(name = "Permiso.findByDescripcionPermiso", query = "SELECT p FROM Permiso p WHERE p.descripcionPermiso = :descripcionPermiso")
    , @NamedQuery(name = "Permiso.findByRolIdRol", query = "SELECT p FROM Permiso p WHERE p.permisoPK.rolIdRol = :rolIdRol")})
public class Permiso implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PermisoPK permisoPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "descripcion_permiso")
    private String descripcionPermiso;
    @JoinColumn(name = "rol_id_rol", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Rol rol;

    public Permiso() {
    }

    public Permiso(PermisoPK permisoPK) {
        this.permisoPK = permisoPK;
    }

    public Permiso(PermisoPK permisoPK, String descripcionPermiso) {
        this.permisoPK = permisoPK;
        this.descripcionPermiso = descripcionPermiso;
    }

    public Permiso(int idPermiso, int rolIdRol) {
        this.permisoPK = new PermisoPK(idPermiso, rolIdRol);
    }

    public PermisoPK getPermisoPK() {
        return permisoPK;
    }

    public void setPermisoPK(PermisoPK permisoPK) {
        this.permisoPK = permisoPK;
    }

    public String getDescripcionPermiso() {
        return descripcionPermiso;
    }

    public void setDescripcionPermiso(String descripcionPermiso) {
        this.descripcionPermiso = descripcionPermiso;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (permisoPK != null ? permisoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Permiso)) {
            return false;
        }
        Permiso other = (Permiso) object;
        if ((this.permisoPK == null && other.permisoPK != null) || (this.permisoPK != null && !this.permisoPK.equals(other.permisoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Permiso[ permisoPK=" + permisoPK + " ]";
    }
    
}
