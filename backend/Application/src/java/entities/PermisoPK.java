/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author usuario
 */
@Embeddable
public class PermisoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_permiso")
    private int idPermiso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rol_id_rol")
    private int rolIdRol;

    public PermisoPK() {
    }

    public PermisoPK(int idPermiso, int rolIdRol) {
        this.idPermiso = idPermiso;
        this.rolIdRol = rolIdRol;
    }

    public int getIdPermiso() {
        return idPermiso;
    }

    public void setIdPermiso(int idPermiso) {
        this.idPermiso = idPermiso;
    }

    public int getRolIdRol() {
        return rolIdRol;
    }

    public void setRolIdRol(int rolIdRol) {
        this.rolIdRol = rolIdRol;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idPermiso;
        hash += (int) rolIdRol;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PermisoPK)) {
            return false;
        }
        PermisoPK other = (PermisoPK) object;
        if (this.idPermiso != other.idPermiso) {
            return false;
        }
        if (this.rolIdRol != other.rolIdRol) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.PermisoPK[ idPermiso=" + idPermiso + ", rolIdRol=" + rolIdRol + " ]";
    }
    
}
