/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Usuarios;
import entities.service.UsuariosFacadeREST;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author usuario
 */
@Stateless
@Path("loginservice")
public class LoginService {
    @PersistenceContext(unitName = "ApplicationPU")
    private EntityManager em;
    
    @POST
    @Path("login/{user}/{pass}")
    @Produces(MediaType.TEXT_PLAIN)
    public String IniciarSesion(@PathParam("user") String user, @PathParam("pass") String pass) {
        String response = "";
        try {
            Usuarios usuario = new Usuarios();

            TypedQuery<Usuarios> query =
                em.createNamedQuery("Usuarios.findByUserName", Usuarios.class);
            query.setParameter("userName", user);
            usuario = query.getSingleResult();
            String password = usuario.getPassword();
            if (password.equalsIgnoreCase(pass)) {
                response = String.format("{'sessionid': 'test', 'nombre': '{0}'}", usuario.getNombre());
            } else {
                response = "{'error': 'contraseña incorrecta'}";
            }
        } catch (NoResultException ex) {
            response= "{ 'error': 'el usuario no existe'}";
        }
        return response;
    }
}
