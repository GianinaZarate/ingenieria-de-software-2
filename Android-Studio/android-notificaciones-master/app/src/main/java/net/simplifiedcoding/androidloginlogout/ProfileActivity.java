package net.simplifiedcoding.androidloginlogout;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.view.View;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    //Textview to show currently logged in user
    private TextView textView;

    private Button verProducto;
    //private Button addProduccto;
    private Button buscarProductos;
    private Button buscarClientes;
    private Button buscarVentas;
    private Button ventas;
    private Button compras;
    private Button buscarCompras;
    private Button buscarCobranza;
    private Button localizacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        //buttons
       // verProducto = (Button) findViewById(R.id.verProducto);
        //addProduccto = (Button) findViewById(R.id.addProduccto);
        buscarProductos = (Button) findViewById(R.id.buscarProductos);
        buscarClientes = (Button) findViewById(R.id.buscarClientes);
        buscarVentas = (Button) findViewById(R.id.buscarVentas);
        ventas = (Button) findViewById(R.id.Ventas);
        compras = (Button) findViewById(R.id.Compras);
        buscarCompras = (Button) findViewById(R.id.buscarCompras);
        buscarCobranza = (Button) findViewById(R.id.buscarCobranza);
        localizacion = (Button) findViewById(R.id.localizacion);

        //verProducto.setOnClickListener(this);
        //addProduccto.setOnClickListener(this);
        buscarProductos.setOnClickListener(this);
        buscarClientes.setOnClickListener(this);
        buscarVentas.setOnClickListener(this);
        ventas.setOnClickListener(this);
        compras.setOnClickListener(this);
        buscarCompras.setOnClickListener(this);
        buscarCobranza.setOnClickListener(this);
        localizacion.setOnClickListener(this);

        //Initializing textview
        textView = (TextView) findViewById(R.id.textView);

        //Fetching email from shared preferences
        SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String email = sharedPreferences.getString(Config.EMAIL_SHARED_PREF,"Not Available");

        //Showing the current logged in email to textview
        textView.setText("Usuario: " + email);
    }

    //Logout function
    private void logout(){
        //Creating an alert dialog to confirm logout
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Esta seguro que quiere salir?");
        alertDialogBuilder.setPositiveButton("Si",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        //Getting out sharedpreferences
                        SharedPreferences preferences = getSharedPreferences(Config.SHARED_PREF_NAME,Context.MODE_PRIVATE);
                        //Getting editor
                        SharedPreferences.Editor editor = preferences.edit();

                        //Puting the value false for loggedin
                        editor.putBoolean(Config.LOGGEDIN_SHARED_PREF, false);

                        //Putting blank value to email
                        editor.putString(Config.EMAIL_SHARED_PREF, "");

                        //Saving the sharedpreferences
                        editor.commit();

                        //Starting login activity
                        Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                });

        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        //Showing the alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menuLogout) {
            logout();
        }
        return super.onOptionsItemSelected(item);
    }


    public void onClick(View v){
        //if(v == verProducto) {
           // Intent intent = new Intent();
           // intent.setClass(ProfileActivity.this, ViewAllEmployee.class);
           // startActivity(intent);
        //}

       // if(v == addProduccto) {
         //   Intent intent = new Intent();
           // intent.setClass(ProfileActivity.this, MainActivity.class);
           // startActivity(intent);
        //}

        if(v == buscarProductos){
            Intent browserIntent =
                    new Intent(Intent.ACTION_VIEW, Uri.parse("http://181.120.255.100/buscarProducto/busqueda_producto.php"));
            startActivity(browserIntent);
        }

        if(v == buscarClientes){
            Intent browserIntent =
                    new Intent(Intent.ACTION_VIEW, Uri.parse("http://181.120.255.100/buscarCliente/busqueda_cliente.php"));
            startActivity(browserIntent);
        }

        if(v == buscarVentas){
            Intent browserIntent =
                    new Intent(Intent.ACTION_VIEW, Uri.parse("http://181.120.255.100/buscarVenta/main.html"));
            startActivity(browserIntent);
        }

        if(v == ventas){
            Intent browserIntent =
                    new Intent(Intent.ACTION_VIEW, Uri.parse("http://181.120.255.100/ventas/index.php"));
            startActivity(browserIntent);
        }

        if(v == compras){
            Intent browserIntent =
                    new Intent(Intent.ACTION_VIEW, Uri.parse("http://181.120.255.100/compras/index.php"));
            startActivity(browserIntent);
        }

        if(v == buscarCompras){
            Intent browserIntent =
                    new Intent(Intent.ACTION_VIEW, Uri.parse("http://181.120.255.100/buscarCompra/main.html"));
            startActivity(browserIntent);
        }

        if(v == buscarCobranza){
            Intent browserIntent =
                    new Intent(Intent.ACTION_VIEW, Uri.parse("http:/181.120.255.100/buscarCobranza/main.html"));
            startActivity(browserIntent);
        }

        if(v == localizacion){
            Intent browserIntent =
                    new Intent(Intent.ACTION_VIEW, Uri.parse("http://181.120.255.100/geolocalizacion/main_geo.html"));
            startActivity(browserIntent);
        }

    }
}

